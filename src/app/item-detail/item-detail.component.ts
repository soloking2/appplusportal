import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from './../message-dialog/message-dialog.component';
import { ZamtnApiService } from './../zamtn-api.service';
import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-item-detail',
  templateUrl: './item-detail.component.html',
  styleUrls: ['./item-detail.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ItemDetailComponent implements OnInit {
  res;
  slideIndex = 1;
  downloadButtonText = "Download";
  constructor(public router: Router, 
    public route: ActivatedRoute,
     public dialog: MatDialog,
      public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService) { }

  ngAfterViewInit() {
    setTimeout(() => {
      this.showSlides(this.slideIndex)
    }, 1000);
  }

  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }

  showSlides(n) {
    var i;
    let slide;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { this.slideIndex = 1 }
    if (n < 1) { this.slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
      slide = slides[i] as HTMLElement;
      slide.style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slide = slides[this.slideIndex - 1] as HTMLElement;
    slide.style.display = "flex";
    dots[this.slideIndex - 1].className += " active";
  }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.getItemDetails(params.itemId);
      });
  }
  getItemDetails(id) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.item_id = id;
        requestHeaderParams.token = token;
        this.mtnApi.getItemDetails(requestHeaderParams, params).then((res: any) => {
          if (res) {
            this.res = res;
            this.res.size = Math.floor(this.res.size * 0.000001) + " MB"
            if (!res.downloaded && res.subscription != 'active') {
              this.downloadButtonText = "Info";
            }
          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }

  openCategory(category) {
    this.router.navigate(['/category-items-list'], { queryParams: { categoryId: category.id, categoryName: category.name } });
  }

  downloadItem() {
    if (this.downloadButtonText == "Info") {
      let dialogRef = this.dialog.open(MessageDialogComponent, {
        data: { dialog_type: 'message', message: this._config.getConfig("exhausted_message") }
      });
    } else {
      this.downloadContentIfAllowed(this.res, this.res.subscription).then((res: any) => {
        if (res.success) {
          window.location.href = res.url;
        }
      }).catch((err) => {
        //SHOW INFO DIALOG
      })
    }
  }

  downloadContentIfAllowed(item: any, subscriptionStatus: any) {
    return new Promise((resolve, reject) => {
      if (item.downloaded) {
        this.download(item, resolve);
      } else {
        if (subscriptionStatus == 'active') {
          this.download(item, resolve);
        } else {
          reject();
        }
      }
    })
  }


  download(item, resolve) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.item_id = item.id;
        requestHeaderParams.token = token;
        this.mtnApi.download(requestHeaderParams, params).then((res: any) => {
          resolve(res)
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }

  rate(item) {
    let dialogRef = this.dialog.open(MessageDialogComponent, {
      data: { dialog_type: 'rating', item: this.res }
    });
  }
}
