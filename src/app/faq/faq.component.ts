import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.css']
})
export class FaqComponent implements OnInit {
  res;
  constructor(public utils: UtilityService, public _config: Config) {
    this.utils.getJSON('../../assets/' + _config.getConfig('store') + '/faq.json').subscribe((res) => {
      this.res = res;
    })
  }

  ngOnInit() {
  }

}
