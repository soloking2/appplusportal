import { MessageDialogComponent } from './../message-dialog/message-dialog.component';
import { MatDialog } from '@angular/material';
import { ZamtnApiService } from './../zamtn-api.service';
import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-my-account',
  templateUrl: './my-account.component.html',
  styleUrls: ['./my-account.component.css']
})
export class MyAccountComponent implements OnInit {

  res;
  currentPage = 1;
  order_by;
  currentCount = 0;
  total_pages: number;
  constructor(public router: Router,
    public route: ActivatedRoute, public utils: UtilityService,
    public _config: Config, public dialog: MatDialog,
    public mtnApi: ZamtnApiService) {
    this.order_by = this.utils.ACCOUNT_PAGE_ORDER_BY_KEYS[0].value;
    this.getAccountDetails();
  }

  ngOnInit() {

  }
  getAccountDetails() {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        requestHeaderParams.token = token;
        params.order_by = this.order_by;
        this.mtnApi.getMyAccount(requestHeaderParams, params, this.currentPage).then((res: any) => {
          if (res) {
            this.res = res;
            this.total_pages = Math.ceil(this.res.count / 12);
            this.currentCount = this.currentPage * 12;
            if (this.currentCount > this.res.count) {
              this.currentCount = this.res.count;
            }
          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }
  openItemsForPageNumber(i) {
    this.currentPage = i + 1;
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage = this.currentPage - 1;
    }
  }

  nextPage() {
    if (this.currentPage < this.total_pages) {
      this.currentPage = this.currentPage + 1;
    }
  }

  onSortOrderChange(event) {
    this.getAccountDetails()
  }
  downloadItem(item) {
    this.downloadContentIfAllowed(item, this.res.subscription).then((res: any) => {
      if (res.success) {
        window.location.href = res.url;
      }
    }).catch((err) => {
      //SHOW INFO DIALOG
    })
  }

  downloadContentIfAllowed(item: any, subscriptionStatus?: any) {
    return new Promise((resolve, reject) => {
      this.download(item, resolve);
    })
  }


  download(item, resolve) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.item_id = item.id;
        requestHeaderParams.token = token;
        this.mtnApi.download(requestHeaderParams, params).then((res: any) => {
          resolve(res)
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }

  rate(item) {
    let dialogRef = this.dialog.open(MessageDialogComponent, {
      data: { dialog_type: 'rating', item: item }
    });
  }

  openItem(item) {
    this.router.navigate(['/detail'], { queryParams: { itemId: item.id } });
  }
}
