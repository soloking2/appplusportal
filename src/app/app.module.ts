import { HttpClientModule } from '@angular/common/http';
import { APP_INITIALIZER, NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatSelectModule } from '@angular/material/select';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { RatingModule } from 'ng-starrating';
import { NgxSpinnerModule } from "ngx-spinner";
import { ToastrModule } from 'ngx-toastr';
import { Config } from '../assets/configurations/Config';
import { AppComponent } from './app.component';
import { appRoutingModule } from './app.routing';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoryItemsListComponent } from './category-items-list/category-items-list.component';
import { FaqComponent } from './faq/faq.component';
import { HomeComponent } from './home/home.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { LoginComponent } from './login/login.component';
import { CustomMaterialModule } from './material.module';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { TermsComponent } from './terms/terms.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    ItemDetailComponent,
    CategoryItemsListComponent,
    CategoriesListComponent,
    MyAccountComponent,
    FaqComponent,
    TermsComponent,
    SearchResultsComponent,
    MessageDialogComponent,
  ],
  imports: [HttpModule,
    HttpClientModule,
    MatSelectModule,
    FormsModule,
    BrowserModule,
    MatDialogModule,
    appRoutingModule,
    NgxSpinnerModule,
    RatingModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MatExpansionModule,
    MatGridListModule,
    CustomMaterialModule,
    ToastrModule.forRoot() // ToastrModule added
  ], entryComponents: [MessageDialogComponent],
  providers: [
    Config,
    { provide: APP_INITIALIZER, useFactory: (config: Config) => () => config.load(), deps: [Config], multi: true },
    { provide: LocationStrategy, useClass: HashLocationStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
