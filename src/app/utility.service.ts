import { ZamtnApiService } from './zamtn-api.service';
import { Config } from './../assets/configurations/Config';
import { map } from 'rxjs/operators';
import { Headers, RequestOptions, Http, Response } from '@angular/http';
import { Injectable } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgxSpinnerService } from "ngx-spinner";
@Injectable({
  providedIn: 'root'
})
export class UtilityService {
  constructor(private http: Http,
    // private authService: AuthService,
    public toastCtrl: ToastrService,
    public loadingCtrl: NgxSpinnerService,
  ) {
  }

  STORAGE_KEYS = {
    TOKEN: "token",
    EXPIRY_ON: "expiry_on"
  }

  ORDER_BY_KEYS = [
    { key: "Latest", value: "-updated_on" },
    { key: "Popularity", value: "-num_views" },
    { key: "ABC", value: "name" },
    { key: "Rating", value: "-rating" },
  ];

  ACCOUNT_PAGE_ORDER_BY_KEYS = [
    { key: "Download Date", value: "-download_on" },
    { key: "ABC", value: "item__name" },
    { key: "Rating", value: "-item__rating" }
  ];

  getJSON(path) {
    return this.http.get(path).pipe(map((response: Response) => response.json()));
  }

  getCSV(filePath) {
    return new Promise((resolve, reject) => {
      this.http.get(filePath)
        .subscribe(
          data => {
            resolve(this.extractData(data));
          },
          error => {
            reject(error);
          }
        );
    });
  }

  extractData(res: Response) {

    let csvData = res['_body'] || '';
    let allTextLines = csvData.split(/\r\n|\n/);
    let headers = allTextLines[0].split(',');
    let lines = [];

    for (let i = 0; i < allTextLines.length; i++) {
      // split content based on comma
      let data = allTextLines[i].split(',');
      if (data.length == headers.length) {
        let tarr = [];
        for (let j = 0; j < headers.length; j++) {
          tarr.push(data[j]);
        }
        lines.push(tarr);
      }
    }
    return lines;
  }



  generateSessionId() { // generates UUID. Public Domain/MIT
    var d = new Date().getTime();
    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
      d += performance.now(); //use high-precision timer if available
    }
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = (d + Math.random() * 16) % 16 | 0;
      d = Math.floor(d / 16);
      return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
  }


  //Storage Code
  saveData(key, value) {
    return localStorage.setItem(key, value);
  }

  getData(key) {
    return new Promise((resolve, reject) => {
      let value = localStorage.getItem(key);
      if (value) {
        resolve(value);
      } else {
        reject();
      }

    })
  }

  deleteData(key) {
    return localStorage.removeItem(key);
  }

  clearData() {
    return localStorage.clear();
  }

  isCordovaAvailable() {

    if (!(<any>window).cordova) {
      return false;
    }
    return true;

  }

  showToast(message: string, error?: boolean): void {
    if (error) {
      this.toastCtrl.error(message, "Error");
    } else {
      this.toastCtrl.success(message, "");
    }

  }


  hideLoading() {
    this.loadingCtrl.hide();
  }

  showLoading() {
    this.loadingCtrl.show();
  }


  makeGetRequest(url, requestHeaders?) {
    return new Promise((resolve, reject) => {
      let headers: any = new Headers();
      if (requestHeaders.token) {
        headers.append('Authorization', 'Token ' + requestHeaders.token);
      }
      // headers.append('content-type', 'application/json; charset=utf-8');
      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.get(url, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });
    })
  }

  makePostRequest(url, params?, requestHeaders?) {

    return new Promise((resolve, reject) => {

      let headers: any = new Headers();
      if (requestHeaders) {
        if (requestHeaders.msisdn) {
          headers.append('msisdn', requestHeaders.msisdn);
        } else {
          headers.append('Authorization', 'Token ' + requestHeaders.token);
        }
      }

      headers.append('content-type', 'application/json; charset=utf-8');
      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.post(url, params, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });


    })

  }

  makePostRequestWithFormData(url, params?, requestHeaders?) {
    return new Promise((resolve, reject) => {
      let headers: any = new Headers();
      if (requestHeaders.msisdn) {
        headers.append('msisdn', requestHeaders.msisdn);
      } else {
        headers.append('Authorization', 'Token ' + requestHeaders.token);
      }
      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.post(url, params, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });
    })
  }

  makePutRequest(url, params?, requestHeaders?, isMultipart?) {
    return new Promise((resolve, reject) => {
      let headers: any = new Headers();
      if (requestHeaders.msisdn) {
        headers.append('msisdn', requestHeaders.msisdn);
      } else {
        headers.append('Authorization', 'Token ' + requestHeaders.token);
      }
      if (!isMultipart) {
        headers.append('Content-Type', 'application/json; charset=utf-8');
      }

      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.put(url, params, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });
    })
  }

  makePutRequestWithFormData(url, params?, requestHeaders?) {
    return new Promise((resolve, reject) => {
      let headers: any = new Headers();
      headers.append('Authorization', 'Token ' + requestHeaders.token);

      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.put(url, params, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });
    })
  }

  makeDeleteRequest(url, requestHeaders?) {
    return new Promise((resolve, reject) => {
      let headers: any = new Headers();
      if (requestHeaders.msisdn) {
        headers.append('msisdn', requestHeaders.msisdn);
      } else {
        headers.append('Authorization', 'Token ' + requestHeaders.token);
      }
      headers.append('content-type', 'application/json; charset=utf-8');
      let requestOptions = (headers == undefined) ? undefined : new RequestOptions({ headers: headers });
      this.http.delete(url, requestOptions)
        .subscribe((res: Response) => {
          if (res.status === 200 || res.status === 201) {
            resolve(res.json());
          }
        }, error => {
          if (error.status == 400) {
            resolve(error.json());
          } else {
            reject(error);
          }
        });
    })
  }
}
