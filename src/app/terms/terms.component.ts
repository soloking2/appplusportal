import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {
  res;
  constructor(public utils: UtilityService, public _config: Config) {
    this.utils.getJSON('../../assets/' + _config.getConfig('store') + '/tnc.json').subscribe((res) => {
      this.res = res;
    })
  }
  ngOnInit() {
  }

}
