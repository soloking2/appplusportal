import { Config } from './../assets/configurations/Config';
import { UtilityService } from './utility.service';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ZamtnApiService {
  zamtn_domain: string;
  constructor(private utils: UtilityService, public _config: Config) {
    this.zamtn_domain = this._config.getConfig('mtn_base_url');
  }


  ///////////////////////////////////////////////////
  ////////// Home Dashboard API's
  ///////////////////////////////////////////////////
  getStores(requestHeaders, params) {
    return new Promise((resolve, reject) => {

      let endPoint = this.zamtn_domain + 'portal/store/';
      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }

  getItemDetails(requestHeaders, params) {
    return new Promise((resolve, reject) => {

      let endPoint = this.zamtn_domain + 'portal/item/';
      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })

  }


  login(reqParams) {
    return new Promise((resolve, reject) => {

      let endPoint = this.zamtn_domain + 'portal/login/';
      this.utils.makePostRequest(endPoint, reqParams)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }
  getCategoryItemsList(requestHeaders, params, pageNumber?) {
    return new Promise((resolve, reject) => {
      var ep = this.zamtn_domain + 'portal/items/';
      if (pageNumber) {
        ep = ep + '?page=' + pageNumber;
      }

      this.utils.makePostRequest(ep, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }

  getCategories(requestHeaders, params) {
    return new Promise((resolve, reject) => {

      let endPoint = this.zamtn_domain + 'portal/categories/';
      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }

  getMyAccount(requestHeaders, params, pageNumber) {
    return new Promise((resolve, reject) => {
      let endPoint = this.zamtn_domain + 'portal/account/';
      if (pageNumber) {
        endPoint = endPoint + '?page=' + pageNumber;
      }
      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }
  search(requestHeaders, params, pageNumber) {
    return new Promise((resolve, reject) => {
      let endPoint = this.zamtn_domain + 'portal/search/';
      if (pageNumber) {
        endPoint = endPoint + '?page=' + pageNumber;
      }

      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }

  download(requestHeaders, params) {
    return new Promise((resolve, reject) => {
      let endPoint = this.zamtn_domain + 'portal/download/';

      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }

  rating(requestHeaders, params) {
    return new Promise((resolve, reject) => {
      let endPoint = this.zamtn_domain + 'portal/rating/';

      this.utils.makePostRequest(endPoint, params, requestHeaders)
        .then((resp: any) => {
          resolve(resp);
        })
        .catch((err) => {
          reject(err);
        })
    })
  }
}
