import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { Config } from '../../assets/configurations/Config';
import { UtilityService } from '../utility.service';
import { ZamtnApiService } from '../zamtn-api.service';
import { MessageDialogComponent } from './../message-dialog/message-dialog.component';

@Component({
  selector: 'app-category-items-list',
  templateUrl: './category-items-list.component.html',
  styleUrls: ['./category-items-list.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CategoryItemsListComponent implements OnInit {
  res;
  category_name;
  order_by;
  category_id;
  currentPage = 1;
  currentCount = 0;
  total_pages: number;
  constructor(public router: Router, public route: ActivatedRoute, public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService, public dialog: MatDialog) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        // Defaults to 0 if no query param provided.
        this.category_id = params.categoryId;
        this.category_name = params.categoryName;
        this.order_by = this.utils.ORDER_BY_KEYS[0].value;
        this.categoryList(this.currentPage);
      });
  }
  categoryList(pageNumber?) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.category_id = this.category_id;
        params.order_by = this.order_by;

        requestHeaderParams.token = token;
        this.mtnApi.getCategoryItemsList(requestHeaderParams, params, pageNumber).then((res: any) => {
          if (res) {
            this.res = res;

            this.total_pages = Math.ceil(this.res.count / 12);
            this.currentCount = pageNumber * 12;
            if (this.currentCount > this.res.count) {
              this.currentCount = this.res.count;
            }
          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }
  openCategory(category) {
    this.router.navigate(['/category-items-list'], { queryParams: { categoryId: category.id, categoryName: category.name } });
  }

  openItemsForPageNumber(i) {
    this.currentPage = i + 1;
    this.categoryList(this.currentPage)
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage = this.currentPage - 1;
      this.categoryList(this.currentPage)
    }
  }

  nextPage() {
    if (this.currentPage < this.total_pages) {
      this.currentPage = this.currentPage + 1;
      this.categoryList(this.currentPage)
    }
  }

  onSortOrderChange(event) {
    this.categoryList(this.currentPage)
  }

  downloadItem(item) {
    this.downloadContentIfAllowed(item, this.res.subscription).then((res: any) => {
      if (res.success) {
        window.location.href = res.url;
      }
    }).catch((err) => {
      //SHOW INFO DIALOG
    })
  }

  openInfo(item) {
    let dialogRef = this.dialog.open(MessageDialogComponent, {
      data: { dialog_type: 'message', message: this._config.getConfig("exhausted_message") }
    });
  }

  downloadContentIfAllowed(item: any, subscriptionStatus: any) {
    return new Promise((resolve, reject) => {
      if (item.downloaded) {
        this.download(item, resolve);
      } else {
        if (subscriptionStatus == 'active') {
          this.download(item, resolve);
        } else {
          reject();
        }
      }
    })
  }


  download(item, resolve) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.item_id = item.id;
        requestHeaderParams.token = token;
        this.mtnApi.download(requestHeaderParams, params).then((res: any) => {
          resolve(res)
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }

  rate(item) {
    let dialogRef = this.dialog.open(MessageDialogComponent, {
      data: { dialog_type: 'rating', item: item }
    });
  }

  openItem(item) {
    this.router.navigate(['/detail'], { queryParams: { itemId: item.id } });
  }
}
