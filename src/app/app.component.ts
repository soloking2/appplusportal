import { Component, ViewChild } from '@angular/core';
import { MatSidenav } from '@angular/material';
import { Router, NavigationEnd } from '@angular/router';
import { Config } from './../assets/configurations/Config';
import { UtilityService } from './utility.service';
import { ZamtnApiService } from './zamtn-api.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  @ViewChild('sidenav') public sidenav: MatSidenav;
  isLoginPage;
  title = 'ZA MTN';
  searchString: String;
  constructor(public router: Router,
    public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService) {
    //this.checkTokenExpiry();
    this.router.events.subscribe((ev) => {
      if (ev instanceof NavigationEnd) {
        if (ev.url.indexOf('login') > 0) {
          this.isLoginPage = true;
        } else {
          this.isLoginPage = false;
        }
      }
    });
    let token = location.href.split('mobile/users/?token=')[1]
    if (token) {
      this.loginWithToken(token);
    } 
    else if(location.href.indexOf('terms') > -1 || location.href.indexOf('faq') > -1){
      // Do Nothing
    }
    else {
      this.checkTokenExpiry()
    }
  }
  loginWithToken(token) {
    let params: any = {};
    params.token = token;
    params.store = this._config.getConfig('store');
    params.partner = this._config.getConfig('partner');
    this.mtnApi.login(params).then((res: any) => {
      if (res.success) {
        this.utils.saveData(this.utils.STORAGE_KEYS.TOKEN, res.token);
        this.utils.saveData(this.utils.STORAGE_KEYS.EXPIRY_ON, res.expiry_on);
        this.router.navigate(['/home']);
      } else {
        this.utils.showToast(res.message, true);
      }
    }).catch((err) => {
      console.log(err);
    })
  }

  checkTokenExpiry() {
    this.utils.getData(this.utils.STORAGE_KEYS.EXPIRY_ON).then((tokenExpiry: any) => {
      if (tokenExpiry) {
        var currentDate = new Date();
        var expiryDate = new Date(tokenExpiry);
        if (expiryDate < currentDate) {
          this.utils.deleteData(this.utils.STORAGE_KEYS.TOKEN);
          this.utils.deleteData(this.utils.STORAGE_KEYS.EXPIRY_ON);
          this.router.navigate(['/login'], { replaceUrl: true });
        } else {
          this.router.navigate(['/home'], { replaceUrl: true });
        }
      } else {
        this.router.navigate(['/login'], { replaceUrl: true });
      }
    }).catch((err) => {
      this.router.navigate(['/login'], { replaceUrl: true });
    })
  }

  search() {
    this.router.navigate(['/search-results'], { queryParams: { searchString: this.searchString } });

  }
  closeDrawer() {
    this.sidenav.close();
  }
  onActivate(event) {
    // this.activeComponent = event.constructor.name;
  }


  openPage(page) {
    this.router.navigate(['/' + page]);
    this.closeDrawer();
  }


  logout() {
    this.utils.deleteData(this.utils.STORAGE_KEYS.TOKEN);
    this.utils.deleteData(this.utils.STORAGE_KEYS.EXPIRY_ON);
    this.router.navigate(['/login'], { replaceUrl: true });
    this.closeDrawer();
  }
}
