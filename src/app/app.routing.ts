import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from '../app/home/home.component';
import { CategoriesListComponent } from './categories-list/categories-list.component';
import { CategoryItemsListComponent } from './category-items-list/category-items-list.component';
import { FaqComponent } from './faq/faq.component';
import { ItemDetailComponent } from './item-detail/item-detail.component';
import { LoginComponent } from './login/login.component';
import { MessageDialogComponent } from './message-dialog/message-dialog.component';
import { MyAccountComponent } from './my-account/my-account.component';
import { SearchResultsComponent } from './search-results/search-results.component';
import { TermsComponent } from './terms/terms.component';


const routes: Routes = [
  { path: 'home', component: HomeComponent },
/*   { path: '', component: LoginComponent }, */
  { path: 'login', component: LoginComponent },
  { path: 'detail', component: ItemDetailComponent },
  { path: 'category-items-list', component: CategoryItemsListComponent },
  { path: 'categories', component: CategoriesListComponent },
  { path: 'my-account', component: MyAccountComponent },
  { path: 'faq', component: FaqComponent },
  { path: 'terms', component: TermsComponent },
  { path: 'search-results', component: SearchResultsComponent },
  { path: 'dialog', component: MessageDialogComponent },
];

export const appRoutingModule = RouterModule.forRoot(routes);
