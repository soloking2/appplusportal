import { MatDialog } from '@angular/material';
import { MessageDialogComponent } from './../message-dialog/message-dialog.component';
import { ZamtnApiService } from './../zamtn-api.service';
import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-search-results',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.css']
})
export class SearchResultsComponent implements OnInit {

  res;
  currentPage = 1;
  currentCount = 0;
  total_pages: number;
  order_by;
  searchString;
  constructor(public route: ActivatedRoute, public dialog: MatDialog, public router: Router, public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService) { }

  ngOnInit() {
    this.route
      .queryParams
      .subscribe(params => {
        this.searchString = params.searchString;
        this.order_by = this.utils.ORDER_BY_KEYS[0].value;
        this.search();
      });
  }



  search() {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.search = this.searchString;
        params.order_by = this.order_by;
        requestHeaderParams.token = token;
        this.mtnApi.search(requestHeaderParams, params, this.currentPage).then((res: any) => {
          if (res) {
            this.res = res
            this.total_pages = Math.ceil(this.res.count / 12);
            this.currentCount = this.currentPage * 12;
            if (this.currentCount > this.res.count) {
              this.currentCount = this.res.count;
            }
          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }
  openItemsForPageNumber(i) {
    this.currentPage = i + 1;
    this.search();
  }

  previousPage() {
    if (this.currentPage > 1) {
      this.currentPage = this.currentPage - 1;
      this.search();
    }
  }

  nextPage() {
    if (this.currentPage < this.total_pages) {
      this.currentPage = this.currentPage + 1;
      this.search();
    }
  }

  onSortOrderChange(event) {
    this.search();
  }

  downloadItem(item) {
    this.downloadContentIfAllowed(item, this.res.subscription).then((res: any) => {
      if (res.success) {
        window.location.href = res.url;
      }
    }).catch((err) => {
      //SHOW INFO DIALOG
    })
  }

  downloadContentIfAllowed(item: any, subscriptionStatus: any) {
    return new Promise((resolve, reject) => {
      if (item.downloaded) {
        this.download(item, resolve);
      } else {
        if (subscriptionStatus == 'active') {
          this.download(item, resolve);
        } else {
          reject();
        }
      }
    })
  }


  download(item, resolve) {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        params.item_id = item.id;
        requestHeaderParams.token = token;
        this.mtnApi.download(requestHeaderParams, params).then((res: any) => {
          resolve(res)
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }


  rate(item) {
    let dialogRef = this.dialog.open(MessageDialogComponent, {
      data: { dialog_type: 'rating', item: this.res }
    });
  }

  openItem(item) {
    this.router.navigate(['/detail'], { queryParams: { itemId: item.id } });
  }
}
