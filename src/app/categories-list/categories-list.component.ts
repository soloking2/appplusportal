import { ZamtnApiService } from './../zamtn-api.service';
import { Config } from './../../assets/configurations/Config';
import { UtilityService } from './../utility.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-categories-list',
  templateUrl: './categories-list.component.html',
  styleUrls: ['./categories-list.component.css']
})
export class CategoriesListComponent implements OnInit {
  res;
  category_name;
  constructor(public router: Router, public route: ActivatedRoute, public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService) {
    this.categoryList();
  }


  ngOnInit() {

  }
  categoryList() {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        requestHeaderParams.token = token;
        this.mtnApi.getCategories(requestHeaderParams, params).then((res: any) => {
          if (res) {
            this.res = res;
            console.log(res);
          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }
  openCategory(category) {
    this.router.navigate(['/category-items-list'], { queryParams: { categoryId: category.id, categoryName: category.name } });
  }
}
