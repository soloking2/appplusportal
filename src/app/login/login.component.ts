import { UtilityService } from './../utility.service';
import { ZamtnApiService } from './../zamtn-api.service';
import { Config } from './../../assets/configurations/Config';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  content_type = "login";
  subscribe_message;
  forgot_password_message;
  constructor(private router: Router, public zamtnApi: ZamtnApiService, public utils: UtilityService, public formBuilder: FormBuilder, public _config: Config) {
    var partner = this._config.getConfig('partner');
    var store = this._config.getConfig('store');
    this.subscribe_message = this._config.getConfig("subscribe_message");
    this.forgot_password_message = this._config.getConfig("forgot_password_message");
    this.loginForm = formBuilder.group({
      partner: [partner],
      store: [store],
      msisdn: [''],
      countrycode: ['+27'],
      password: [''],
      token: ['']
    });
  }

  ngOnInit() {
  }


  login() {
    var countrycode = this.loginForm.controls['countrycode'].value;
    countrycode = countrycode.replace("+", "");
    let params = this.loginForm.value;
    params.msisdn = countrycode + this.loginForm.controls['msisdn'].value
    this.zamtnApi.login(params).then((res: any) => {
      if (res.success) {
        this.utils.saveData(this.utils.STORAGE_KEYS.TOKEN, res.token);
        this.utils.saveData(this.utils.STORAGE_KEYS.EXPIRY_ON, res.expiry_on);
        this.router.navigate(['/home']);
      } else {
        this.utils.showToast(res.message, true);
      }
    }).catch((err) => {
      console.log(err);
    })
  }

  subscribe() {
    this.content_type = "subscribe";
  }

  forgotPassword() {
    this.content_type = "forgot_password";
  }

  back() {
    this.content_type = "login";
  }
}
