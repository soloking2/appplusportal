import { Router } from '@angular/router';
import { Config } from './../../assets/configurations/Config';
import { ZamtnApiService } from './../zamtn-api.service';
import { UtilityService } from './../utility.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {

  slideIndex = 1;
  constructor(public router: Router, public utils: UtilityService, public _config: Config, public mtnApi: ZamtnApiService) {
  }

  plusSlides(n) {
    this.showSlides(this.slideIndex += n);
  }

  currentSlide(n) {
    this.showSlides(this.slideIndex = n);
  }

  showSlides(n) {
    var i;
    let slide;
    var slides = document.getElementsByClassName("mySlides");
    var dots = document.getElementsByClassName("dot");
    if (n > slides.length) { this.slideIndex = 1 }
    if (n < 1) { this.slideIndex = slides.length }
    for (i = 0; i < slides.length; i++) {
      slide = slides[i] as HTMLElement;
      slide.style.display = "none";
    }
    for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
    }
    slide = slides[this.slideIndex - 1] as HTMLElement;
    slide.style.display = "flex";
    dots[this.slideIndex - 1].className += " active";
  }

  res: any;

  ngOnInit() {
    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.store = this._config.getConfig('store');
        requestHeaderParams.token = token;
        this.mtnApi.getStores(requestHeaderParams, params).then((res: any) => {
          if (res) {
            this.res = res;

          }
        }).catch((err) => {
          console.log(err);
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
      }
    }).catch((err) => {
      this.utils.showToast("Something went wrong", true);
    })
  }

  ngAfterViewInit() {
    setTimeout(() => {
      this.showSlides(this.slideIndex)
    }, 1000);
  }

  openItem(item) {
    this.router.navigate(['/detail'], { queryParams: { itemId: item.id } });
  }

  openCategory(category) {
    this.router.navigate(['/category-items-list'], { queryParams: { categoryId: category.id, categoryName: category.name } });
  }

}
