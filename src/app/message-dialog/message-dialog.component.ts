import { ZamtnApiService } from './../zamtn-api.service';
import { UtilityService } from './../utility.service';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Component, OnInit, ViewEncapsulation, Inject } from '@angular/core';
@Component({
  selector: 'app-message-dialog',
  templateUrl: './message-dialog.component.html',
  styleUrls: ['./message-dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MessageDialogComponent implements OnInit {
  dialog_type;
  item;
  message;
  rating = 0;
  constructor(public dialog: MatDialog, @Inject(MAT_DIALOG_DATA) public data: any, public utils: UtilityService, public mtnApi: ZamtnApiService) {

    this.dialog_type = data.dialog_type;
    this.item = data.item;
    this.message = data.message;
  }

  ngOnInit() {

  }



  done() {

    this.utils.getData(this.utils.STORAGE_KEYS.TOKEN).then((token: string) => {
      if (token) {
        let requestHeaderParams: any = {};
        let params: any = {};
        params.item_id = this.item.id;
        params.rating = this.rating;
        requestHeaderParams.token = token;
        this.mtnApi.rating(requestHeaderParams, params).then((res: any) => {
          if (res.success) {
            this.utils.showToast(res.message)
          }
          this.close();
        }).catch((err) => {
          console.log(err);
          this.close();
        })
      } else {
        this.utils.showToast("User Not loggedin", true);
        this.close();
      }
    }).catch((err) => {
      this.close();
      this.utils.showToast("Something went wrong", true);
    })
  }

  close() {
    this.dialog.closeAll()
  }

  onRate(event) {
    this.rating = event.newValue;
  }

}
