import { TestBed } from '@angular/core/testing';

import { ZamtnApiService } from './zamtn-api.service';

describe('ZamtnApiService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ZamtnApiService = TestBed.get(ZamtnApiService);
    expect(service).toBeTruthy();
  });
});
